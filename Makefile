run:
	docker-compose up 

migrate:
	docker-compose run web ./manage.py makemigrations && ./manage.py migrate

startapp:
	docker-compose run web ./manage.py startapp testapp

